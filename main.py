import json
import enum
import re
import csv
import requests
import urllib.request
import os

csvdata = []
header = []
uniqueKeys = []
primarykeyList = []
successList = []
failureList = []

defaultCsvURL = "http://vedant.me/files/PeakProject/ip.csv";
defaultJsonURL = "http://vedant.me/files/PeakProject/rules.json";

urllib.request.urlretrieve(defaultCsvURL, "ip.csv")
with open('ip.csv') as csv_file :
    csv_reader = csv.reader(csv_file, delimiter=',')
    rowcount = 0
    for row in csv_reader:
        if rowcount == 0 :
            rowcount = 1
            header.append(row)
        else :
            csvdata.append(row)

with urllib.request.urlopen("http://vedant.me/files/PeakProject/rules.json") as url:
    ipjsonDict = json.loads(url.read().decode())

successList.append(header[0])
header[0].append("errordescription")
failureList.append(header[0])

rules = ipjsonDict['columns']
primaryKeyName = ipjsonDict['primarykey']
primarykeyIndex = header[0].index(primaryKeyName)

for index in range(len(csvdata)):
    primarykeyList.append(csvdata[index][primarykeyIndex])

for key in primarykeyList:
    if key not in uniqueKeys:
        uniqueKeys.append(key)
    elif key in uniqueKeys:
        uniqueKeys.remove(key)

def validateDatatype(datatype,value):
    if 'varchar' in datatype:
        temp = re.findall(r'\d+', datatype)
        res = list(map(int, temp))
        if int(res[0]) < len(value):
            return -1
        return 1
    elif 'integer' in datatype: # integer range for python is different than spark
        try:
            intval = int(value)
            if intval not in range( -2147483648, 2147483647) :  # assuming 32 bit compiler
                return -2
            return 1
        except Exception as e:
            return -3
    elif 'decimal' in datatype:
        try:
            decimalval = float(value)
            temp = re.findall(r'\d+', datatype)
            res = list(map(int, temp))
            if '.' in value :
                if len(value.split('.')[0]) > res[0] or len(value.split('.')[1]) > res[1] :
                    # print('Decimal size not matching')
                    return -4
            else :
                if len(value) > res[0] :
                    return -4
            return 1
        except Exception as e:
            # print('not a float',e)
            return -4

def validateMandatory(data,value):
    if data :
        if value == '' or value.strip() == '':
            return 0
    return 1

def lineRuleChecker(csvdataLine):
    if csvdataLine[primarykeyIndex] not in uniqueKeys:
        # goes to failure file
        csvdataLine.append("Primary key validation failure")
        failureList.append(csvdataLine)
        # print(csvdataLine)
        return
    for index in range(len(csvdataLine)):
        # print("For index ", index, " value is " ,csvdataLine[index])
        for k,v in rules[index]['rules'].items():
            if k == 'datatype':
                result = validateDatatype(v,csvdataLine[index])
                if  result == 1:
                    continue
                else :
                    if result == -1:
                        csvdataLine.append("Varchar DataType not valid for " + header[0][index])
                    if result == -2:
                        csvdataLine.append("Integer DataType not valid for " + header[0][index])
                    if result == -3:
                        csvdataLine.append("Integer out of range for " + header[0][index])
                    if result == -4:
                        csvdataLine.append("Decimal DataType not valid for " + header[0][index])
                    failureList.append(csvdataLine)
                    return
            elif  k == 'ismandatory':
                if validateMandatory(v,csvdataLine[index]) :
                    continue
                else :
                    # goes to failure file
                    csvdataLine.append("Missing mandatory value for " + header[0][index])
                    failureList.append(csvdataLine)
                    return
    successList.append(csvdataLine)
    # print(csvdataLine)

for csvdataLine in csvdata:
     lineRuleChecker(csvdataLine)

with open('output/success.csv', 'w') as successFile:
    writerS = csv.writer(successFile)
    writerS.writerows(successList)

with open('output/failure.csv', 'w') as failureFile:
    writerF = csv.writer(failureFile)
    writerF.writerows(failureList)

os.remove("ip.csv")
successFile.close()
failureFile.close()
