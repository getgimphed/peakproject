## PEAK PROJECT TO VALIDATE CSV AGAINST RULES

####  Pre-requisite
###### Installed Python-3
###### Active internet connection

####  The project has one file 'main.py' which loads rules from json placed at Server : 'http://vedant.me/files/PeakProject/rules.json'
####  and validates the input csv file placed at  Server : 'http://vedant.me/files/PeakProject/ip.csv'
####
####  Workflow
####  1. Run main.py in cmd.
####  2. Loading both, Rules and CSV file from server; it validates the CSV
####  3. Generates two output files in 'Output' Directory : 'success.csv' and 'failure.csv'
####  
#### In order to customize the input files
#### Change variables '''defaultCsvURL''' and '''defaultJsonURL''' to required server path.

#### Problem statement

##### We need to validate the data of the csv file.The rule set is given in json format. We need to validate the data accordingly to the rules given.
##### If any of the validation fails for a record (row) then we will not be processing that record further, just discard it in the very first validation failure.
##### In Output we need to create two files one is error file, which is having all the records of failure with the failure description and other file having successful records.
##### File is place on cloud storage (like aws s3) and output file also place on could storage (like aws s3).
##### Rule set example:
##### e.g,
##### For varchar we need to ensure the length of data should not be more than the given length.
##### For 'ismandatory' we need to ensure there should not be null values in this.
##### For integer only numbers are allowed, similarly primary key should be validated for the primary key constraints.
##### Rule set :
##### schemaRules='{"tablename":"customer","columns":[{"columnname":"userid","rules":{"datatype":"varchar(255)","ismandatory":true}},{"columnname":"useremailaddress","rules":{"datatype":"varchar(255)"}},{"columnname":"totalwishlistitems","rules":{"datatype":"integer","ismandatory":true}},{"columnname":"transtotalrefunded","rules":{"datatype":"decimal(22,4)"}}],"primarykey":"userid"}'

###### Sample Data:
##### userid,useremailaddress,totalwishlistitems,transtotalrefunded
##### 1,a@gmail.com,20,200.2234
##### 2,"a bc@gmail.com",40,400
##### ,"c@gmail.com",5,78.98
##### 4,null,344567897987,78654467383689.876
##### 5,ra@peak.ai,,78654467383689.876
##### 10,"a bc@gmail.com",670,44500
##### 12,"a bc@gmail.com",9840,48700.8
##### 2,"xbc@gmail.com",4220,4900.98967
##### 13,cert@peak.ai,,7809E356E.98
#####
###### Output :
#####
######  Failure File :
##### userid,useremailaddress,totalwishlistitems,transtotalrefunded,errordescription
##### ,"c@gmail.com",5,78.98, primary key validation failure
##### 4,null,344567897987,78654467383689.876, integer out of range for totalwishlistitems
##### 5,ra@peak.ai,,78654467383689.876, missing mandatory value for totalwishlistitems
##### 2,"a bc@gmail.com",40,400, primary key validation fail
##### 2,"xbc@gmail.com",4220,4900.98967, primary key validation failure
##### 13,cert@peak.ai,,7809E356E.98, missing mandatory value for totalwishlistitems (or  transtotalrefunded not a number  which ever  validation is first)

######  Success File:
##### userid,useremailaddress,totalwishlistitems,transtotalrefunded
##### 1,a@gmail.com,20,200.2234
##### 10,"a bc@gmail.com",670,44500
##### 12,"a bc@gmail.com",9840,48700.8
